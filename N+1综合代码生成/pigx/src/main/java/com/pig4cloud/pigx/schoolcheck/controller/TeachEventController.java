/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.schoolcheck.entity.TeachEvent;
import com.pig4cloud.pigx.schoolcheck.service.TeachEventService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:05
 */
@RestController
@AllArgsConstructor
@RequestMapping("/teachevent" )
@Api(value = "teachevent", tags = "N+1学校考勤信息管理管理")
public class TeachEventController {

    private final  TeachEventService teachEventService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param teachEvent N+1学校考勤信息管理
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    public R getTeachEventPage(Page page, TeachEvent teachEvent) {
        return R.ok(teachEventService.page(page, Wrappers.query(teachEvent)));
    }


    /**
     * 通过id查询N+1学校考勤信息管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(teachEventService.getById(id));
    }

    /**
     * 新增N+1学校考勤信息管理
     * @param teachEvent N+1学校考勤信息管理
     * @return R
     */
    @ApiOperation(value = "新增N+1学校考勤信息管理", notes = "新增N+1学校考勤信息管理")
    @SysLog("新增N+1学校考勤信息管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_teachevent_add')" )
    public R save(@RequestBody TeachEvent teachEvent) {
        return R.ok(teachEventService.save(teachEvent));
    }

    /**
     * 修改N+1学校考勤信息管理
     * @param teachEvent N+1学校考勤信息管理
     * @return R
     */
    @ApiOperation(value = "修改N+1学校考勤信息管理", notes = "修改N+1学校考勤信息管理")
    @SysLog("修改N+1学校考勤信息管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_teachevent_edit')" )
    public R updateById(@RequestBody TeachEvent teachEvent) {
        return R.ok(teachEventService.updateById(teachEvent));
    }

    /**
     * 通过id删除N+1学校考勤信息管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除N+1学校考勤信息管理", notes = "通过id删除N+1学校考勤信息管理")
    @SysLog("通过id删除N+1学校考勤信息管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('schoolcheck_teachevent_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(teachEventService.removeById(id));
    }

}
