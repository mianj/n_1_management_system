/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:05
 */
@Data
@TableName("n_one_teach_event")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "N+1学校考勤信息管理")
public class TeachEvent extends Model<TeachEvent> {
private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId
    @ApiModelProperty(value="自增Id")
    private Integer id;
    /**
     * 教学班级Id
     */
    @ApiModelProperty(value="教学班级Id")
    private Integer teachClassId;
    /**
     * 事件名
     */
    @ApiModelProperty(value="事件名")
    private String eventName;
    /**
     * 提示语
     */
    @ApiModelProperty(value="提示语")
    private String markedWords;
    /**
     * 教学事件类型
     */
    @ApiModelProperty(value="教学事件类型")
    private String eventType;
    /**
     * 占所属评分比例
     */
    @ApiModelProperty(value="占所属评分比例")
    private Double scorePercentage;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime gmtCreate;
    /**
     * 终止时间
     */
    @ApiModelProperty(value="终止时间")
    private LocalDateTime gmtEnd;
    /**
     * 最后修改时间
     */
    @ApiModelProperty(value="最后修改时间")
    private LocalDateTime gmtModified;
    /**
     * 最后修改人
     */
    @ApiModelProperty(value="最后修改人")
    private String userModified;
    /**
     * 租户Id
     */
    @ApiModelProperty(value="租户Id",hidden=true)
    private Integer tenantId;
    }
