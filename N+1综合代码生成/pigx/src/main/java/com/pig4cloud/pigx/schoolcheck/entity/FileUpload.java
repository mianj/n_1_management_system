/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:47
 */
@Data
@TableName("n_one_file_upload")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "N+1学校考勤信息管理")
public class FileUpload extends Model<FileUpload> {
private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId
    @ApiModelProperty(value="自增Id")
    private Integer id;
    /**
     * 学生学工号
     */
    @ApiModelProperty(value="学生学工号")
    private String stuCode;
    /**
     * 教学事件Id
     */
    @ApiModelProperty(value="教学事件Id")
    private Integer teachEventId;
    /**
     * 教师学工号
     */
    @ApiModelProperty(value="教师学工号")
    private String teacherCode;
    /**
     * 文件位置
     */
    @ApiModelProperty(value="文件位置")
    private String filePosition;
    /**
     * 描述
     */
    @ApiModelProperty(value="描述")
    private String description;
    /**
     * 上传时间
     */
    @ApiModelProperty(value="上传时间")
    private LocalDateTime gmtCreate;
    /**
     * 最后修改时间
     */
    @ApiModelProperty(value="最后修改时间")
    private LocalDateTime gmtModified;
    /**
     * 最后修改人
     */
    @ApiModelProperty(value="最后修改人")
    private String userModified;
    /**
     * 租户Id
     */
    @ApiModelProperty(value="租户Id",hidden=true)
    private Integer tenantId;
    }
