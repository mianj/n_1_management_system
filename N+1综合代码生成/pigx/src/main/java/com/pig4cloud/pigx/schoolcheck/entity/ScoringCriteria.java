/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:33
 */
@Data
@TableName("n_one_scoring_criteria")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "N+1学校考勤信息管理")
public class ScoringCriteria extends Model<ScoringCriteria> {
private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId
    @ApiModelProperty(value="自增Id")
    private Integer int;
    /**
     * 考勤比例
     */
    @ApiModelProperty(value="考勤比例")
    private Double attendanceProportion;
    /**
     * 课堂表现比例
     */
    @ApiModelProperty(value="课堂表现比例")
    private Double performanceProportion;
    /**
     * 作业比例
     */
    @ApiModelProperty(value="作业比例")
    private Double homeworkProportion;
    /**
     * 平时测验比例
     */
    @ApiModelProperty(value="平时测验比例")
    private Double ordinaryTestProportion;
    /**
     * 期中考试比例
     */
    @ApiModelProperty(value="期中考试比例")
    private Double midTermProportion;
    /**
     * 实验比例
     */
    @ApiModelProperty(value="实验比例")
    private Double experimentProportion;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime gmtCreate;
    /**
     * 最后修改时间
     */
    @ApiModelProperty(value="最后修改时间")
    private LocalDateTime gmtModified;
    /**
     * 最后修改人
     */
    @ApiModelProperty(value="最后修改人")
    private String userModified;
    /**
     * 租户ID
     */
    @ApiModelProperty(value="租户ID",hidden=true)
    private Integer tenantId;
    }
