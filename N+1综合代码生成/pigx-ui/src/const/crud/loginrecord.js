export const tableOption = {
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  "column": [
	  {
      "type": "input",
      "label": "自增Id",
      "prop": "id"
    },	  {
      "type": "input",
      "label": "学工号",
      "prop": "userId"
    },	  {
      "type": "input",
      "label": "登陆时间",
      "prop": "gmtCreat"
    },	  {
      "type": "input",
      "label": "最后修改时间",
      "prop": "gmtModified"
    },	  {
      "type": "input",
      "label": "最后修改人",
      "prop": "userModified"
    },	  {
      "type": "input",
      "label": "租户ID",
      "prop": "tenantId"
    }  ]
}
