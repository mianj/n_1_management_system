package com.example.uploadingfiles;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

/**
 * @Auther: Ball
 * @Date: 2020/3/13 0:18
 */
@Data
public class Test {

    @CsvBindByName(column = "code")
    private String code;

    @CsvBindByName(column = "姓名")
    private String username;

    @CsvBindByName(column = "roleName")
    private String roleName;

    @CsvBindByName(column = "phone")
    private String phone;

    @CsvBindByName(column = "policeClassName")
    private String policeClassName;
}
