/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.16 : Database - n_one_system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`n_one_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `n_one_system`;

/*Table structure for table `n_one_course` */

DROP TABLE IF EXISTS `n_one_course`;

CREATE TABLE `n_one_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `course_name` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '课程名',
  `credite` tinyint(2) DEFAULT NULL COMMENT '学分',
  `describe` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '描述',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='课程';

/*Data for the table `n_one_course` */

/*Table structure for table `n_one_event_submit` */

DROP TABLE IF EXISTS `n_one_event_submit`;

CREATE TABLE `n_one_event_submit` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `teach_event_idevent_id` int(11) DEFAULT NULL COMMENT '教学事件Id',
  `stu_code` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学生学工号',
  `submit_item` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '提交markdown',
  `score` int(4) DEFAULT NULL COMMENT '评分',
  `event_type` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教学事件类型',
  `evaluate_markdown` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '评价markdown',
  `file_id` int(11) DEFAULT NULL COMMENT '上传文件记录Id',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '提交时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '评分时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='教学事件提交';

/*Data for the table `n_one_event_submit` */

/*Table structure for table `n_one_file_upload` */

DROP TABLE IF EXISTS `n_one_file_upload`;

CREATE TABLE `n_one_file_upload` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `stu_code` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学生学工号',
  `teach_event_id` int(11) DEFAULT NULL COMMENT '教学事件Id',
  `teacher_code` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教师学工号',
  `file_position` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件位置',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '描述',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件上传记录';

/*Data for the table `n_one_file_upload` */

/*Table structure for table `n_one_login_record` */

DROP TABLE IF EXISTS `n_one_login_record`;

CREATE TABLE `n_one_login_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `user_id` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学工号',
  `gmt_creat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登陆时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='登录记录\r\n';

/*Data for the table `n_one_login_record` */

/*Table structure for table `n_one_police_class` */

DROP TABLE IF EXISTS `n_one_police_class`;

CREATE TABLE `n_one_police_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `class_name` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '行政班级名',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT '11' COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='行政班级';

/*Data for the table `n_one_police_class` */

/*Table structure for table `n_one_scoring_criteria` */

DROP TABLE IF EXISTS `n_one_scoring_criteria`;

CREATE TABLE `n_one_scoring_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `attendance_proportion` double DEFAULT NULL COMMENT '考勤比例',
  `performance_proportion` double DEFAULT NULL COMMENT '课堂表现比例',
  `homework_proportion` double DEFAULT NULL COMMENT '作业比例',
  `ordinary_test_proportion` double DEFAULT NULL COMMENT '平时测验比例',
  `mid_term_proportion` double DEFAULT NULL COMMENT '期中考试比例',
  `experiment_proportion` double DEFAULT NULL COMMENT '实验比例',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='评分标准\n\n';

/*Data for the table `n_one_scoring_criteria` */

/*Table structure for table `n_one_teach_class` */

DROP TABLE IF EXISTS `n_one_teach_class`;

CREATE TABLE `n_one_teach_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `teacher_code` char(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教师编码',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  `teach_class_name` char(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教学班级名称',
  `start_date` date DEFAULT NULL COMMENT '开课时间',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='教学班级表';

/*Data for the table `n_one_teach_class` */

/*Table structure for table `n_one_teach_event` */

DROP TABLE IF EXISTS `n_one_teach_event`;

CREATE TABLE `n_one_teach_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `teach_class_id` int(11) DEFAULT NULL COMMENT '教学班级Id',
  `event_name` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件名',
  `marked_words` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '提示语',
  `event_type` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教学事件类型',
  `score_percentage` double DEFAULT NULL COMMENT '占所属评分比例',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_end` datetime DEFAULT NULL COMMENT '终止时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='教学事件';

/*Data for the table `n_one_teach_event` */

/*Table structure for table `n_one_teach_stu` */

DROP TABLE IF EXISTS `n_one_teach_stu`;

CREATE TABLE `n_one_teach_stu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增Id',
  `teach_class_id` int(11) DEFAULT NULL COMMENT '教学班级id',
  `score_proportion_id` int(11) DEFAULT NULL COMMENT '评分标准id',
  `stu_code` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学生学工号',
  `class_name` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '行政班级名',
  `gmt_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  `user_modified` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最后修改人',
  `tenant_id` int(11) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='教学班级学生表';

/*Data for the table `n_one_teach_stu` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
