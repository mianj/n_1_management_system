/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.schoolcheck.entity.PoliceClass;
import com.pig4cloud.pigx.schoolcheck.service.PoliceClassService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * 行政班级
 *
 * @author wmx
 * @date 2020-02-19 15:19:33
 */
@RestController
@AllArgsConstructor
@RequestMapping("/policeclass" )
@Api(value = "policeclass", tags = "行政班级管理")
public class PoliceClassController {

    private final  PoliceClassService policeClassService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param policeClass 行政班级
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    public R getPoliceClassPage(Page page, PoliceClass policeClass) {
        return R.ok(policeClassService.page(page, Wrappers.query(policeClass)));
    }


    /**
     * 通过id查询行政班级
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(policeClassService.getById(id));
    }

    /**
     * 新增行政班级
     * @param policeClass 行政班级
     * @return R
     */
    @ApiOperation(value = "新增行政班级", notes = "新增行政班级")
    @SysLog("新增行政班级" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_policeclass_add')" )
    public R save(@RequestBody PoliceClass policeClass) {
        return R.ok(policeClassService.save(policeClass));
    }

    /**
     * 修改行政班级
     * @param policeClass 行政班级
     * @return R
     */
    @ApiOperation(value = "修改行政班级", notes = "修改行政班级")
    @SysLog("修改行政班级" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_policeclass_edit')" )
    public R updateById(@RequestBody PoliceClass policeClass) {
        return R.ok(policeClassService.updateById(policeClass));
    }

    /**
     * 通过id删除行政班级
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除行政班级", notes = "通过id删除行政班级")
    @SysLog("通过id删除行政班级" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('schoolcheck_policeclass_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(policeClassService.removeById(id));
    }

	/**
	 * 上传csv文件添加行政班级
	 * @param multipartFile
	 * @return
	 */
	@SysLog("上传csv文件添加行政班级")
	@PostMapping("/save/csvfile")
	public R user(@RequestParam("file") MultipartFile multipartFile) {
		System.out.println("获取文件:"+multipartFile);
		return R.ok(policeClassService.savePoliceClassByCSV(multipartFile));
	}

}
