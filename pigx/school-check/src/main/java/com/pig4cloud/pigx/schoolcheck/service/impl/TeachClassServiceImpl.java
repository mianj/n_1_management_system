/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.schoolcheck.service.impl;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.admin.api.dubbo.SysUserServiceDubbo;
import com.pig4cloud.pigx.admin.api.entity.SysUser;
import com.pig4cloud.pigx.schoolcheck.entity.Course;
import com.pig4cloud.pigx.schoolcheck.entity.TeachClass;
import com.pig4cloud.pigx.schoolcheck.mapper.TeachClassMapper;
import com.pig4cloud.pigx.schoolcheck.service.CourseService;
import com.pig4cloud.pigx.schoolcheck.service.TeachClassService;
import com.pig4cloud.pigx.schoolcheck.utile.CSVFileUtiles;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:42:55
 */
@Service
public class TeachClassServiceImpl extends ServiceImpl<TeachClassMapper, TeachClass> implements TeachClassService {

	@Reference(version = "1.0.0")
	private SysUserServiceDubbo sysUserServiceDubbo;

	@Autowired
	private CourseService courseService;

	@Override
	public Boolean saveTeachClassByCSV(MultipartFile multipartFile) {
		List<TeachClass> teachClasses = null;
		try {
			teachClasses = CSVFileUtiles.StoreAnd2Arr(multipartFile, TeachClass.class);
		} catch (IOException e) {
			return false;
		}
		System.out.println("准话："+teachClasses.toString());
		teachClasses.forEach(teachClass -> {
			SysUser sysUser = sysUserServiceDubbo.getByName(teachClass.getTeacherName());
			teachClass.setTeacherCode(sysUser.getCode());
			Course course = courseService.getByName(teachClass.getCourseName());
			teachClass.setCourseId(course.getId());
		});
		return saveBatch(teachClasses);
	}

	@Override
	public TeachClass getByTeaNameAndCourseName(String teacherName, String courseName) {
		QueryWrapper<TeachClass> queryWrapper = new QueryWrapper<TeachClass>();
		queryWrapper.eq("teacher_name",teacherName);
		queryWrapper.eq("course_name",courseName);
		return baseMapper.selectOne(queryWrapper);
	}
}
