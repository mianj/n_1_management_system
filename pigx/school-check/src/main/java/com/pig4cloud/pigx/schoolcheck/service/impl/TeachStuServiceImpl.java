/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.schoolcheck.service.impl;

import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvParser;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.admin.api.dubbo.SysUserServiceDubbo;
import com.pig4cloud.pigx.admin.api.entity.SysUser;
import com.pig4cloud.pigx.schoolcheck.entity.TeachClass;
import com.pig4cloud.pigx.schoolcheck.entity.TeachStu;
import com.pig4cloud.pigx.schoolcheck.mapper.TeachStuMapper;
import com.pig4cloud.pigx.schoolcheck.service.TeachClassService;
import com.pig4cloud.pigx.schoolcheck.service.TeachStuService;
import com.pig4cloud.pigx.schoolcheck.utile.CSVFileUtiles;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:02
 */
@Service
public class TeachStuServiceImpl extends ServiceImpl<TeachStuMapper, TeachStu> implements TeachStuService {

	@Reference(version = "1.0.0")
	private SysUserServiceDubbo sysUserServiceDubbo;

	@Autowired
	private TeachClassService teachClassService;

	@Override
	public Boolean saveTeachStuByCSV(MultipartFile multipartFile) {
		List<TeachStu> teachStus = null;
		try {
			teachStus = CSVFileUtiles.StoreAnd2Arr(multipartFile, TeachStu.class);
		} catch (IOException e) {
			return false;
		}
		for (TeachStu teachStu:
		teachStus) {
			SysUser sysUser = sysUserServiceDubbo.getByName(teachStu.getStuName());
			teachStu.setStuCode(sysUser.getCode());
			teachStu.setClassName(sysUser.getPoliceClassName());
			TeachClass teachClass = teachClassService.getByTeaNameAndCourseName(teachStu.getTeacherName(), teachStu.getCourseName());
			teachStu.setTeachClassId(teachClass.getId());
			teachStu.setTeacherCode(teachClass.getTeacherCode());
		}
		return saveBatch(teachStus);
	}
}
