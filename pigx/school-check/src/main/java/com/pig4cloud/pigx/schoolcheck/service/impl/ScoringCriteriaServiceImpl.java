/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.schoolcheck.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.schoolcheck.entity.ScoringCriteria;
import com.pig4cloud.pigx.schoolcheck.mapper.ScoringCriteriaMapper;
import com.pig4cloud.pigx.schoolcheck.service.ScoringCriteriaService;
import org.springframework.stereotype.Service;

/**
 * 评分标准


 *
 * @author wmx
 * @date 2020-02-19 15:19:12
 */
@Service
public class ScoringCriteriaServiceImpl extends ServiceImpl<ScoringCriteriaMapper, ScoringCriteria> implements ScoringCriteriaService {

}
