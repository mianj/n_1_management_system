/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.schoolcheck.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.admin.api.dubbo.SysUserServiceDubbo;
import com.pig4cloud.pigx.admin.api.entity.SysUser;
import com.pig4cloud.pigx.schoolcheck.entity.PoliceClass;
import com.pig4cloud.pigx.schoolcheck.mapper.PoliceClassMapper;
import com.pig4cloud.pigx.schoolcheck.service.PoliceClassService;
import com.pig4cloud.pigx.schoolcheck.utile.CSVFileUtiles;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 行政班级
 *
 * @author wmx
 * @date 2020-02-19 15:19:33
 */
@Service
public class PoliceClassServiceImpl extends ServiceImpl<PoliceClassMapper, PoliceClass> implements PoliceClassService {

	@Reference(version = "1.0.0")
	private SysUserServiceDubbo sysUserServiceDubbo;

	@Override
	public Boolean savePoliceClassByCSV(MultipartFile multipartFile) {
		List<PoliceClass> policeClasses = null;
		try {
			policeClasses = CSVFileUtiles.StoreAnd2Arr(multipartFile, PoliceClass.class);
		} catch (IOException e) {
			return false;
		}
		for (PoliceClass policeClass
				:policeClasses) {
			SysUser sysUser = sysUserServiceDubbo.getByName(policeClass.getTeacherName());
			policeClass.setTeacherCode(sysUser.getCode());
		}
		return saveBatch(policeClasses);
	}
}
