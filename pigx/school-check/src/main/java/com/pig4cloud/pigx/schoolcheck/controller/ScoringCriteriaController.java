/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.schoolcheck.entity.Course;
import com.pig4cloud.pigx.schoolcheck.entity.ScoringCriteria;
import com.pig4cloud.pigx.schoolcheck.service.CourseService;
import com.pig4cloud.pigx.schoolcheck.service.ScoringCriteriaService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 评分标准


 *
 * @author wmx
 * @date 2020-02-19 15:19:12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/scoringcriteria" )
@Api(value = "scoringcriteria", tags = "评分标准管理")
public class ScoringCriteriaController {

    private final ScoringCriteriaService scoringCriteriaService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param scoringCriteria 评分标准
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    public R getScoringCriteriaPage(Page page, ScoringCriteria scoringCriteria) {
        return R.ok(scoringCriteriaService.page(page, Wrappers.query(scoringCriteria)));
    }


    /**
     * 通过id查询评分标准
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(scoringCriteriaService.getById(id));
    }

    /**
     * 新增评分标准
     * @param scoringCriteria 评分标准
     * @return R
     */
    @ApiOperation(value = "新增评分标准 ", notes = "新增评分标准")
    @SysLog("新增评分标准")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_scoringcriteria_add')" )
    public R save(@RequestBody ScoringCriteria scoringCriteria) {
        return R.ok(scoringCriteriaService.save(scoringCriteria));
    }

    /**
     * 修改评分标准
     * @param scoringCriteria 评分标准
     * @return R
     */
    @ApiOperation(value = "修改评分标准", notes = "修改评分标准")
    @SysLog("修改评分标准")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('schoolcheck_scoringcriteria_edit')" )
    public R updateById(@RequestBody ScoringCriteria scoringCriteria) {
        return R.ok(scoringCriteriaService.updateById(scoringCriteria));
    }

    /**
     * 通过id删除评分标准
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除评分标准 ", notes = "通过id删除评分标准")
    @SysLog("通过id删除评分标准 " )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('schoolcheck_scoringcriteria_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(scoringCriteriaService.removeById(id));
    }

}
