/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.schoolcheck.entity.TeachClass;
import org.springframework.web.multipart.MultipartFile;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:42:55
 */
public interface TeachClassService extends IService<TeachClass> {

	/**
	 * 上传csv文件添加教学班级
	 * @param multipartFile
	 * @return
	 */
	Boolean saveTeachClassByCSV(MultipartFile multipartFile);

	/**
	 * 根据老师名和课程名查询教学班级
	 * @param teacherName
	 * @param courseName
	 * @return
	 */
	TeachClass getByTeaNameAndCourseName(String teacherName,String courseName);
}
