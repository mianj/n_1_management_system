/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.schoolcheck.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:02
 */
@Data
@TableName("n_one_teach_stu")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "N+1学校考勤信息管理")
public class TeachStu extends Model<TeachStu> {
private static final long serialVersionUID = 1L;

    /**
     * 自增Id
     */
    @TableId
    @ApiModelProperty(value="自增Id")
    private Integer id;


	/**
	 * 教学老师学工号
	 */
	@ApiModelProperty(value="教学老师学工号")
	private String teacherCode;

	/**
	 * 教学老师姓名
	 */
	@ApiModelProperty(value="教学老师姓名")
	private String teacherName;
	/**
	 * 课程名称
	 */
	@ApiModelProperty(value="课程名称")
	private String courseName;


    /**
     * 教学班级id
     */
    @ApiModelProperty(value="教学班级id")
    private Integer teachClassId;
    /**
     * 评分标准id
     */
    @ApiModelProperty(value="评分标准id")
    private Integer scoreProportionId;
    /**
     * 学生学工号
     */
    @ApiModelProperty(value="学生学工号")
    private String stuCode;

	/**
	 * 学生姓名
	 */
	@ApiModelProperty(value="学生姓名")
	private String stuName;

    /**
     * 行政班级名
     */
    @ApiModelProperty(value="行政班级名")
    private String className;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime gmtCreate;
    /**
     * 最后修改时间
     */
    @ApiModelProperty(value="最后修改时间")
    private LocalDateTime gmtModified;
    /**
     * 最后修改人
     */
    @ApiModelProperty(value="最后修改人")
    private String userModified;
    /**
     * 租户Id
     */
    @ApiModelProperty(value="租户Id",hidden=true)
    private Integer tenantId;
    }
