/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.schoolcheck.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.schoolcheck.entity.Course;
import com.pig4cloud.pigx.schoolcheck.mapper.CourseMapper;
import com.pig4cloud.pigx.schoolcheck.service.CourseService;
import com.pig4cloud.pigx.schoolcheck.utile.CSVFileUtiles;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * N+1学校考勤信息管理
 *
 * @author wmx
 * @date 2020-02-19 12:43:24
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {

	@Override
	public Boolean saveCourseByCSV(MultipartFile multipartFile) {
		List<Course> list = null;
		try {
			list = CSVFileUtiles.StoreAnd2Arr(multipartFile, Course.class);
		} catch (IOException e) {
			return false;
		}
		return saveBatch(list);
	}

	@Override
	public Course getByName(String courseName) {
		QueryWrapper<Course> queryWrapper = new QueryWrapper<Course>();
		queryWrapper.eq("course_name",courseName);
		return getOne(queryWrapper);
	}
}
