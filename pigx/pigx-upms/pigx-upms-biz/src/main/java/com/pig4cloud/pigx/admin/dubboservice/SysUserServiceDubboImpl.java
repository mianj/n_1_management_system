package com.pig4cloud.pigx.admin.dubboservice;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pig4cloud.pigx.admin.api.dubbo.SysUserServiceDubbo;
import com.pig4cloud.pigx.admin.api.entity.SysUser;
import com.pig4cloud.pigx.admin.service.SysUserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Auther: Ball
 * @Date: 2020/2/22 15:26
 */
@Service(version = "1.0.0")
public class SysUserServiceDubboImpl implements SysUserServiceDubbo {

	@Autowired
	private SysUserService sysUserService;

	@Override
	public SysUser getByName(String username) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("userName",username);
		return sysUserService.getOne(queryWrapper);
	}
}
