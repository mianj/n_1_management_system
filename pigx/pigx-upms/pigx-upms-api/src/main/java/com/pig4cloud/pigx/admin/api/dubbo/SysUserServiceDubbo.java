package com.pig4cloud.pigx.admin.api.dubbo;

import com.pig4cloud.pigx.admin.api.entity.SysUser;

/**
 * 用户的dubbo服务
 *
 * @Auther: Ball
 * @Date: 2020/2/22 15:23
 */
public interface SysUserServiceDubbo {

	/**
	 *
	 * @param username
	 * @return
	 */
	SysUser getByName(String username);
}
