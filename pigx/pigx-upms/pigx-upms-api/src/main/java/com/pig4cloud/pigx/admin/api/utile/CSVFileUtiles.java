package com.pig4cloud.pigx.admin.api.utile;

import cn.hutool.core.text.csv.CsvUtil;
import com.alibaba.fastjson.JSONArray;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.pig4cloud.pigx.admin.api.dto.UserDTO;
import com.pig4cloud.pigx.admin.api.utile.storage.StorageService;
import com.sun.org.apache.bcel.internal.classfile.Constant;
import lombok.experimental.UtilityClass;
import okhttp3.internal.http2.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Auther: Ball
 * @Date: 2020/2/20 10:38
 */
@Component
public class CSVFileUtiles {


	private static StorageService storageService;

	@Autowired
	public CSVFileUtiles(StorageService storageService){
		CSVFileUtiles.storageService = storageService;
	}

	/**
	 * 获取文件列表 返回结果为文件名列表
	 * @return
	 * @throws IOException
	 */
	public static List<String> listUploadedFilesName() throws IOException {

		List<String> serveFilesName = storageService.loadAll()
				.map(
						path -> path.getFileName().toString()
				).collect(Collectors.toList());
		return serveFilesName;
	}

	/**
	 *通过文件名获取进行文件获取
	 *
	 * @param filename
	 * @return
	 */
	public static Resource serveFile(String filename) {
		return storageService.loadAsResource(filename);
	}

	/**
	 * 存储文件
	 * @param file
	 */
	public static void storeFile(MultipartFile file){
		storageService.store(file);
	}

	/**
	 * 将文件进行存储，然后解析成对应的数组对象
	 * @param file
	 * @param tClass
	 * @return
	 * @throws IOException
	 */
	public static List StoreAnd2Arr(MultipartFile file,Class tClass) throws IOException {
		String str = new String(file.getBytes());
		storageService.store(file);
		List list = paseCsvToArr(file, tClass);
		return list;
	}

	/**
	 * 解析csv文件并转成bean
	 * @param file csv文件
	 * @param clazz 类
	 * @param <T> 泛型
	 * @return 泛型bean集合
	 */
	public static <T> List<T> paseCsvToArr(MultipartFile file, Class<T> clazz) {
		InputStreamReader in = null;
		try {
			in = new InputStreamReader(getInputStream(file.getInputStream()), "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}

		HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
		strategy.setType(clazz);

		CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(in)
				.withSeparator(',')
				.withQuoteChar('\'')
				.withMappingStrategy(strategy).build();
		return csvToBean.parse();
	}


	/**
	 * 读取流中前面的字符，看是否有bom，如果有bom，将bom头先读掉丢弃
	 *
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static InputStream getInputStream(InputStream in) throws IOException {
		PushbackInputStream testin = new PushbackInputStream(in);
		int ch = testin.read();
		if (ch != 0xEF) {
			testin.unread(ch);
		} else if ((ch = testin.read()) != 0xBB) {
			testin.unread(ch);
			testin.unread(0xef);
		} else if ((ch = testin.read()) != 0xBF) {
			throw new IOException("错误的UTF-8格式文件");
		} else {
		}
		return testin;
	}
}
